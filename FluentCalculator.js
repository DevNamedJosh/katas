class Magic {
    constructor(parent) {
        console.log(parent);
        if(typeof parent !== 'undefined') {
            this.parent = parent;
            this.parent.stack.push(this);
        } else {
            this.parent = null;
            this.stack = [];
        }
    }
    valueOf() {
        if(this.parent !== null) {
            return this.parent.valueOf();
        } else {
            let freshSlicedStack = this.stack.slice();
            if(freshSlicedStack.length % 2 !== 0 || !(this instanceof MagicNumber)) {
                let total = this.value;
                while(freshSlicedStack.length !== 0) {
                    let operator = freshSlicedStack.pop();
                    let number = freshSlicedStack.stack.pop();
                    if (!(operator instanceof MagicOperator && number instanceof MagicNumber)) return NaN;
                    total = operator.operation(total, number);
                }
                return total;
            } else {
                // must be odd
                return NaN;
            }
        }
    }
}

class MagicOperator extends Magic {
    constructor(operation, parent) {
        super(parent);
        console.log(parent);
        this.operation = operation;
    }

    // one through ten
    get zero() {
        return new MagicNumber(0, this);
    }
    get one() {
        console.log(this);
        return new MagicNumber(1, this);
    }
    get two() {
        return new MagicNumber(2, this);
    }
    get three() {
        return new MagicNumber(3, this);
    }
    get four() {
        return new MagicNumber(4, this);
    }
    get five() {
        return new MagicNumber(5, this);
    }
    get six() {
        return new MagicNumber(6, this);
    }
    get seven() {
        return new MagicNumber(7, this);
    }
    get eight() {
        return new MagicNumber(8, this);
    }
    get nine() {
        return new MagicNumber(9, this);
    }
    get ten() {
        return new MagicNumber(10, this);
    }
}

class MagicNumber extends Magic {

    constructor(value, parent) {
        super(parent);
        this.value = value;
    }

    // operations are reversed
    get plus() {
        return new MagicOperator((a, b) => b + a, this);
    }
    get minus() {
        return new MagicOperator((a, b) => b - a, this);
    }

    get times() {
        return new MagicOperator((a, b) => b * a, this);
    }

    get dividedBy() {
        return new MagicOperator((a, b) => b / a, this);
    }
}

let FluentCalculator = new MagicOperator((a, b) => b);

console.log(FluentCalculator.one.plus.five.plus.ten.times.three.valueOf());
