const aCode = 97;
class ChessBoardCell {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.isValidCell = this.isValidCell.bind(this);
        this.copyAndTranslate = this.copyAndTranslate.bind(this);
    }

    isValidCell() {
        return this.x >= 1 && this.x <= 8 && this.y >= 1 && this.y <= 8;
    }

    static fromString(cellStr) {
        return new ChessBoardCell(cellStr.charCodeAt(0) - aCode + 1, parseInt(cellStr.charAt(1)));
    }


    copyAndTranslate(translation) {
        return new ChessBoardCell(this.x+translation[0], this.y+translation[1])
    }
}
function chessKnight(cell) {
    cell = ChessBoardCell.fromString(cell); // from string to chessBoardCell
    console.log(cell)
    let knightPossibilites = [
        [1,-2],
        [2,-1],
        [2,1],
        [1,2],
        [-1,2],
        [-2,1],
        [-2,-1],
        [-1,-2]
    ];
    let knightCells = knightPossibilites.map(cell.copyAndTranslate);
    console.log(knightCells);
    knightCells.forEach((knight) => {
        console.log(knight.isValidCell());
    });
    return knightCells.filter(knight => knight.isValidCell(knight)).length;
}



console.log(chessKnight("a1"));
