const DIRECTIONS = {
    NORTH: 0,
    SOUTH: 1,
    EAST: 2,
    WEST: 4
};

function findWrongWayCow(field) {
    // wrangle them up
    let cows = extractAllCows(field);


    // an arrow function that finds all cows facing a certain direction
    let filterCows = (directionKey) => cows.filter(cow => cow.direction === DIRECTIONS[directionKey]);

    // Turn DIRECTIONS into an array of its keys, fill an array with the result of filterCows called on each directionKey in new array
    // This  will be simpler in ES7 with Object.values()
    let cowsByDirections = Object.keys(DIRECTIONS).map(filterCows);

    // Find the array of cows with one cow, then get that cow out of the array
    let wrongWayCow = cowsByDirections.filter(sameWayCows => sameWayCows.length === 1)[0][0];

    return wrongWayCow.headPoint;
}

function extractAllCows(field) {
    let cows = [];

    // iterate over every point in the field
    for (let y = 0; y < field.length; y++) {
        for (let x = 0; x < field[y].length; x++) {
            let cow = extractCow([x, y], field);
            // extract cow returns false if cow is invalid
            // all objects are true
            if (cow) cows.push(cow);
        }
    }

    return cows;
}
// returns first cow found, or false if none found
function extractCow(headPoint, field) {
    for (let direction in DIRECTIONS) {
        // convert from key to value
        direction = DIRECTIONS[direction];
        let oPoint = translatePoint(headPoint, direction);
        let wPoint = translatePoint(oPoint, direction);
        if (pointValidAndEqual(oPoint, field, "o") && pointValidAndEqual(wPoint, field, "w")) {
            return {headPoint: headPoint, direction: direction};
        }
    }
    return false;
}

// disregards bounds
function translatePoint(point, direction) {
    let [x, y] = point;
    switch (direction) {
        case DIRECTIONS.NORTH:
            y -= 1;
            break;
        case DIRECTIONS.SOUTH:
            y += 1;
            break;
        case DIRECTIONS.EAST:
            x += 1;
            break;
        case DIRECTIONS.WEST:
            x -= 1;
            break;
    }
    return [x, y];
}

// check if point is within bounds of field, then check if it matches the value argument
function pointValidAndEqual(point, field, value) {
    // save typing and add clarity
    let [x, y] = point;
    let columnCount = field[0].length;
    let rowCount = field.length;

    // is the point in bounds? if not, return false
    if (!(x >= 0 && x < columnCount && y >= 0 && y < rowCount)) return false;
    // is the point equal to the value?
    // y and x are flipped: [row, column]
    return field[y][x] === value;
}

